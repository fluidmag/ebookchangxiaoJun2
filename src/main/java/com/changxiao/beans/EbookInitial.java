package com.changxiao.beans;

import java.io.Serializable;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

@Named
@RequestScoped
public class EbookInitial implements Serializable {

    private String selectedEbook;

    public String getSelectedEbook() {
        return selectedEbook;
    }

    public String changeEbook(String newValue) {
        selectedEbook = newValue;
        return selectedEbook;
    }
}
