package com.changxiao.beans;

import com.changxiao.beans.exceptions.NonexistentEntityException;
import com.changxiao.beans.exceptions.RollbackFailureException;
import com.changxiao.entities.Ebook;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;

/**
 *
 * modified from JPAGlassFish03
 */
@Named
@SessionScoped
public class EbookJpaController implements Serializable {

    @Resource
    private UserTransaction userTransaction;

    @PersistenceContext(unitName = "ebooks")
    private EntityManager entityManager;

    /**
     * Default constructor
     */
    public EbookJpaController() {
    }

    /**
     * Take a new or detached entity and add it as a new record in the table
     * 
     * @param ebook
     * @throws RollbackFailureException
     * @throws Exception 
     */
    public void create(Ebook ebook) throws RollbackFailureException, Exception {
        try {
            userTransaction.begin();
            entityManager.persist(ebook);
            userTransaction.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                userTransaction.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    /**
     * Take a detached entity and update the matching record in the table
     * 
     * @param ebook
     * @throws NonexistentEntityException
     * @throws RollbackFailureException
     * @throws Exception 
     */
    public void edit(Ebook ebook) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            userTransaction.begin();
            ebook = entityManager.merge(ebook);
            userTransaction.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                userTransaction.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String isbn = ebook.getIsbn();
                if (findEbook(isbn) == null) {
                    throw new NonexistentEntityException("The ebook with isbn " + isbn + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    /**
     * Delete the record that matched the primary key. Verify that the record exists before deleting it.
     * 
     * @param id
     * @throws NonexistentEntityException
     * @throws RollbackFailureException
     * @throws Exception 
     */
    public void destroy(String isbn) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            userTransaction.begin();
            Ebook ebook;
            try {
                ebook = entityManager.getReference(Ebook.class, isbn);
                ebook.getIsbn();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ebook with isbn " + isbn + " no longer exists.", enfe);
            }
            entityManager.remove(ebook);
            userTransaction.commit();
        } catch (NotSupportedException | SystemException | NonexistentEntityException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                userTransaction.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    /**
     * Return all the records in the table
     * 
     * @return 
     */
    public List<Ebook> findEbook() {
        return findEbook(true, -1, -1);
    }

    /**
     * Return some of the records from the table. Useful for paginating.
     * 
     * @param maxResults
     * @param firstResult
     * @return 
     */
    public List<Ebook> findEbook(int maxResults, int firstResult) {
        return findEbook(false, maxResults, firstResult);
    }

    /**
     * Either find all or find a group of ebook
     * 
     * @param all True means find all, false means find subset
     * @param maxResults Number of records to find
     * @param firstResult Record number to start returning records
     * @return 
     */
    private List<Ebook> findEbook(boolean all, int maxResults, int firstResult) {
            CriteriaQuery cq = entityManager.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Ebook.class));
            Query q = entityManager.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
    }

    /**
     * Find a record by primary key
     * 
     * @param isbn
     * @return 
     */
    public Ebook findEbook(String isbn) {
            return entityManager.find(Ebook.class, isbn);
    }

    /**
     * Return the number of records in the table
     * 
     * @return 
     */
    public int getEbookCount() {
            CriteriaQuery cq = entityManager.getCriteriaBuilder().createQuery();
            Root<Ebook> rt = cq.from(Ebook.class);
            cq.select(entityManager.getCriteriaBuilder().count(rt));
            Query q = entityManager.createQuery(cq);
            System.out.println("ebook count: " + ((Long) q.getSingleResult()).intValue());
            return ((Long) q.getSingleResult()).intValue();
    }
}
