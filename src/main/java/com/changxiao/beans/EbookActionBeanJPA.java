package com.changxiao.beans;

import com.changxiao.entities.Ebook;
import java.io.Serializable;
import java.util.List;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * A magic bean that returns a list of e-books
 *
 * modified from FishActionBean.java@ken
 *
 */
@Named
@RequestScoped
public class EbookActionBeanJPA implements Serializable {

    @PersistenceContext(unitName = "ebooks")
    private EntityManager em;
    
    public List<Ebook> getAll() {
        
        // Object oriented criteria builder
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Ebook> cq = cb.createQuery(Ebook.class);
        Root<Ebook> fish = cq.from(Ebook.class);
        cq.select(fish);
        TypedQuery<Ebook> query = em.createQuery(cq);

        // Using a named query
//        TypedQuery<Fish> query =  entityManager.createNamedQuery("EbookEntity.findAll", EbookEntity.class);

        // Execute the query
        List<Ebook> ebooks = query.getResultList();

        return ebooks;
    }
}
