package com.changxiao.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * This doc has been modified from:   Fish.java@author omni__000
 */
@Entity
@Table(name = "EBOOKS", catalog = "INVENTORY", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EbookEntity.findAll", query = "SELECT f FROM Inventory f"),
    @NamedQuery(name = "EbookEntity.findByIsbn", query = "SELECT f FROM Inventory f WHERE f.isbn = :isbn"),
    @NamedQuery(name = "EbookEntity.findByTitle", query = "SELECT f FROM Inventory f WHERE f.title = :title"),
    @NamedQuery(name = "EbookEntity.findByAuthors", query = "SELECT f FROM Inventory f WHERE f.authors = :authors"),
    @NamedQuery(name = "EbookEntity.findByGenre", query = "SELECT f FROM Inventory f WHERE f.genre = :genre"),
    @NamedQuery(name = "EbookEntity.findByPages", query = "SELECT f FROM Inventory f WHERE f.pages = :pages")
})
public class Ebook implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Size(min=1,max=40)
    @Column(name = "ISBN", length=13)
    private String isbn;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "TITLE", length = 45)
    private String title;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "AUTHORS", length = 40)
    private String authors;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "GENRE", length = 12)
    private String genre;
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PAGES")
    private Integer pages;    
  

    public Ebook() {
    }

    public Ebook(String isbn) {
        this.isbn = isbn;
    }

    public Ebook(String id, String title, String authors, String genre, int pages) {
        this.isbn = id;
        this.title = title;
        this.authors = authors;
        this.genre = genre;
        this.pages = pages;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String id) {
        this.isbn = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (isbn != null ? isbn.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ebook)) {
            return false;
        }
        Ebook other = (Ebook) object;
        if ((this.isbn == null && other.isbn != null) || (this.isbn != null && !this.isbn.equals(other.isbn))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.changxiao.entities.Ebook[Author, Title, Page, Genr, ISBN=" 
                + authors+","+title+","+pages+","+genre+","+ isbn + " ]";
    }

}
